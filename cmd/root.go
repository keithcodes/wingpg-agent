// Copyright © 2019 NAME HERE <EMAIL ADDRESS>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"context"
	"fmt"
	"log"
	"os"
	"syscall"
	"time"

	"github.com/getsentry/sentry-go"
	"github.com/microsoft/go-winio"
	homedir "github.com/mitchellh/go-homedir"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var cfgFile string

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "wingpg-agent",
	Short: "An SSH agent supporting GPG keys natively on Windows",
	Long:  ``,
	// Uncomment the following line if your bare application
	// has an action associated with it:
	Run: func(cmd *cobra.Command, args []string) {
		sentry.Init(sentry.ClientOptions{
			Dsn: "https://b7f1e36edf1947469451d988090e7974@sentry.io/1488277",
		})

		// Get the current value of the ssh socket
		oldSocket := os.Getenv("SSH_AUTH_SOCK")

		// Get the pipe to use for wingpg-agent
		socket, err := cmd.Flags().GetString("socket")
		if err != nil {
			log.Println("error getting socket value")
			log.Println("\t", err.Error())
			sentry.CaptureException(err)
			panic(err)
		}

		// bool to check the status of if an existing agent was running
		existingAgentRunning := false

		if oldSocket != "" {
			log.Printf("checking if an ssh-agent is running on SSH_AUTH_SOCK=%s\n", oldSocket)

			// Attempt to dial the pipe
			timeout := 2 * time.Second
			l, err := winio.DialPipe(oldSocket, &timeout)

			// If there was an error then another agent isn't listening, or there was an i/o error
			if err != nil {
				if err == syscall.ENOENT {
					log.Println("socket doesn't exist, no existing agent running")
				} else if err == winio.ErrTimeout || err == context.DeadlineExceeded {
					log.Println("timed out connecting, no existing agent running")
				} else if err == syscall.ERROR_ACCESS_DENIED {
					log.Println("access denied while dialing pipe, couldn't determine if an agent is already running")
				} else {
					log.Println("an unexpected error occurred dialing the agent pipe")
					log.Println("\t", err.Error())
					sentry.CaptureException(err)
					panic(err)
				}
			} else { // If there wasn't an error, then there is an existing agent listening that will be overridden
				defer l.Close()

				// An existing instance of wingpg-agent is probably already running!
				if oldSocket == socket {
					log.Println("warning: There is an existing agent already running, and it has the specified wingpg-agent pipe so it is highly likely that it's another instance of wingpg-agent.")
					log.Printf("          We cannot override an agent already running on the specified pipe \"%s\", so you must close the existing agent on that pipe before starting this agent.\n", socket)
					log.Panicln("         This wingpg-agent will now close as it cannot bind to a pipe that is already in use.")
					os.Exit(1)
				} else { // The existing agent is probably a different agent
					log.Println("warning: an ssh agent is already running, wingpg-agent will change SSH_AUTH_SOCK to override this agent")
					existingAgentRunning = true
					err = l.Close()
					if err != nil {
						log.Println("error closing connection to existing agent")
						log.Println("\t", err.Error())
						sentry.CaptureException(err)
						panic(err)
					}
				}
			}

		}

		// Ensure the variable for the ssh auth socket is set to the socket we intend to use
		log.Println("setting SSH_AUTH_SOCK to", socket)
		err = os.Setenv("SSH_AUTH_SOCK", socket)
		if err != nil {
			log.Println("error setting SSH_AUTH_SOCK to", socket)
			log.Println("\t", err.Error())
			sentry.CaptureException(err)
			panic(err)
		}

		// We'll reset the ssh socket variable after closing the agent
		// in case the user wants to continue using their previous agent
		if oldSocket != "" && existingAgentRunning == true {
			defer func() error {
				log.Printf("resetting SSH_AUTH_SOCK to it's existing value of \"%s\" so you can continue to use your previous agent\n", oldSocket)
				return os.Setenv("SSH_AUTH_SOCK", oldSocket)
			}()
		}

	},
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		sentry.CaptureException(err)
		os.Exit(1)
	}
}

func init() {
	cobra.OnInitialize(initConfig)

	// Here you will define your flags and configuration settings.
	// Cobra supports persistent flags, which, if defined here,
	// will be global for your application.
	rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is $HOME/.wingpg-agent.yaml)")

	// Cobra also supports local flags, which will only run
	// when this action is called directly.
	rootCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")

	rootCmd.Flags().StringP("socket", "s", `\\.\pipe\wingpg-agent`, "Socket to bind the ssh agent to")
}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	if cfgFile != "" {
		// Use config file from the flag.
		viper.SetConfigFile(cfgFile)
	} else {
		// Find home directory.
		home, err := homedir.Dir()
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}

		// Search config in home directory with name ".wingpg-agent" (without extension).
		viper.AddConfigPath(home)
		viper.SetConfigName(".wingpg-agent")
	}

	viper.AutomaticEnv() // read in environment variables that match

	// If a config file is found, read it in.
	if err := viper.ReadInConfig(); err == nil {
		fmt.Println("Using config file:", viper.ConfigFileUsed())
	}
}
