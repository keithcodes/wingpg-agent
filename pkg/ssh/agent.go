package ssh

// Agent for providing GPG and regular SSH keys
type Agent struct {
}

func NewAgent() *Agent {
	return &Agent{}
}

// List returns the identities known to the agent.
func (a *Agent) List() ([]*Key, error) {

}

// Sign has the agent sign the data using a protocol 2 key as defined
// in [PROTOCOL.agent] section 2.6.2.
func (a *Agent) Sign(key ssh.PublicKey, data []byte) (*ssh.Signature, error) {

}

// Add adds a private key to the agent.
func (a *Agent) Add(key AddedKey) error {

}

// Remove removes all identities with the given public key.
func (a *Agent) Remove(key ssh.PublicKey) error {

}

// RemoveAll removes all identities.
func (a *Agent) RemoveAll() error {

}

// Lock locks the agent. Sign and Remove will fail, and List will empty an empty list.
func (a *Agent) Lock(passphrase []byte) error {

}

// Unlock undoes the effect of Lock
func (a *Agent) Unlock(passphrase []byte) error {

}

// Signers returns signers for all the known keys.
func (a *Agent) Signers() ([]ssh.Signer, error) {

}
